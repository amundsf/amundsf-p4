import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import PokemonCard from '../components/PokemonCard';
import {SEARCH_POKEMONS} from '../graphql/get-pokemons';
import {useQuery} from '@apollo/react-hooks';
import {Pokemon} from "../types/types";
import { useRecoilValue } from "recoil";
import { capturedFilterState, searchState, typeState } from '../atoms/atoms';


export const PokemonsContainer = () => {

    // hooks state init
    const searchText = useRecoilValue(searchState)
    const typeText = useRecoilValue(typeState)
    const capturedState = useRecoilValue(capturedFilterState)
    const [limit, setLimit] = useState(15)
    const [skip, setSkip] = useState(0)

    const {loading, data} = useQuery(SEARCH_POKEMONS, {
        fetchPolicy: "no-cache",
        variables: {
            searchInput: {
                searchText: searchText,
                typeText: typeText,
                limit: limit,
                skip: skip,
                captured: capturedState
            }
        }
    })

    const updateLabelAndSkip = (prevOrNext: string) => { // pagination 
        
       if (prevOrNext === "next"){
           setSkip(skip + 15)
           
       } else {
           setSkip(skip - 15)
       }
    }

    useEffect(() => { // pagination
        setSkip(0)
        setLimit(15)
    }, [searchText, typeText, capturedState])
    
    if (loading){
        return (
            <Text>Please wait..</Text>
        )
    } else if (data.searchPokemon === 0) {
        <Text>No matches for this search</Text>

    }
    else{
        return (
            <View style={styles.container}>
            {data.searchPokemon && data.searchPokemon.map((pokemon: Pokemon) =>
                <PokemonCard key={pokemon.id} pokemon={pokemon}/>)
            }


            <View style  = {styles.paginationContainers}>
            {  
            skip >= 15 &&
            <TouchableOpacity style={styles.ButtonPrevTouch} onPress={() => updateLabelAndSkip("prev")}>
            <Image style = {styles.prevButton} source={require('../resources/button.png')}/>
            </TouchableOpacity>}
            {
            data.searchPokemon.length >= 15 &&
            <TouchableOpacity  style={styles.ButtonNextTouch} onPress={() => updateLabelAndSkip("next")}>
            <Image style = {styles.nextButton} source={require('../resources/button.png')}/>
            </TouchableOpacity>}
            </View>
           
        </View>
 
        )
    }

  
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        maxWidth: "100%",
        minWidth: "85%",
        minHeight: "30%",
        maxHeight: "60%",
       

    },
    prevButton: {
        width: "100%",
        height: "100%"

    },
    nextButton: {
        width: "100%",
        height: "100%"

    },
    ButtonNextTouch: {
        width: 50,
        height: 50,
        color: "white"

    },
    ButtonPrevTouch: {
        width: 50,
        height: 50,
        color: "white",
        transform: [           
            {scaleX: -1} //horizontally flip for previous button
        ]
    },
    paginationContainers: {
        flex: 1, 
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: 15,
        marginRight: 10,
        marginLeft: 10
    }

});

export default PokemonsContainer