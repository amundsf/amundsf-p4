import gql from 'graphql-tag';

export const SEARCH_POKEMONS = gql `
    query Query($searchInput: SearchPokemon!) {
        searchPokemon(searchInput: $searchInput) {
            id
            name
            captured
            imgUrl
            pokemonTypes
        }
    }
`

export const MUTATE_POKEMON = gql `
mutation Mutation($pokemonId: SetCapturedPokemon!) {
  setCaptured(pokemonId: $pokemonId) {
    id
    name
    captured
    imgUrl
    pokemonTypes
  }
}
`
