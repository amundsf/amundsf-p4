# Prosjekt 4 Dokumentasjon

I dette prosjektet har jeg valgt å jobbe alene, og utvikle en react native applikasjon som bruker backenden til prosjekt 3, og lignende design som mobil størrelsen til webapplikasjonen. 

## Forutsetninger 
1. For å kunne se prosjektet på iphone eller android, må du være koblet til
Cisco AnyConnect VPN, på både mobilen din og pc'n med samme nett.
2. Så på mobilen laster du ned CISCO, og Expo GO. 
3. Deretter cd root folder (Prosjekt-4), skriv "npm install", deretter "expo start". Da blir du sendt til en localhost som inneholder en QR-Scan kode som du kan scanne deg inn med mobil appen Expo GO, også vil du få tilgang til siden. 
Hvis du bruker Apple, så må du bruke LAN, android bruker man Tunnel som man ser over QR koden. 

![expo_start](./documentation_images/QR-Scan.PNG)

Andre løsninger finnes også, men dette er det jeg brukte for å se appen. 

## HovedFokus for design og utviklingen av appen
Denne appen er ment til å være en relativ lik versjon av nettleser versjonen av prosjekt 3 når man bruker en mobil horisontalt. Begge deler mange likheter i design delen. 

## Teknologi - GraphQL, Apollo, Recoil, global tilstand og React native funksjoner
Jeg lagde appen ved å bruke EXPO CLI, siden dette var et krav.
Jeg brukte mye av det samme teknologien fra frontend i prosjekt 3 inkludert (graphql, apollo, recoil, global state), ettersom vi fant ut at det var en bra løsning fra forrige prosjekt. 

Jeg fortsatt å bruke apollo for å kommunisere med graphQL, og recoil for å gjøre det enklere å bruke hooks i filene. 


Utfordringen var å adaptere meg fra reactjs til react native. 
I prosjekt 3 da vi brukte reactjs lagde vi søke og filtrerings elementene med Material UI. I react native måtte jeg da droppe dette siden det ikke er lagd for react native. Isteden brukte jeg SelectDropdown og TextInput for å visualisere det. I prosjekt 3 brukte vi et react-icons/fa biblioteket, men siden dette ikke finnes for react native så valgte jeg å fokusere på selve siden, siden det biblioteket ble brukt til å legge til en "to-the-top" knapp, som ikke var så viktig å ha med. 

## Design 
Som sagt ville jeg lage et nokså likt design. Jeg valgte å bruke SafeAreView i App.tsx, sånn at alt som er på applikasjonen ikke kan gå ut av skjermen. Dette løser også problemet med at hvis man prøver å dra seg lenger over skjermen enn ment så vil man ikke se en hvit bakgrunn. Når det kom til pokemon bildene valgte jeg å bruke resizeMode: 'containt', sånn at bildene ikke gikk ut av posisjon sånn at bildet ble kuttet. 

Siden apple og android har helt forskjellige standarder når det kommer til fonts, valgte jeg å gjøre det enkelt å bruke standard font som funker for begge. 
