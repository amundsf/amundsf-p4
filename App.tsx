import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import PokemonsContainer from './containers/PokemonsContainer';
import {ApolloClient, ApolloProvider, InMemoryCache} from "@apollo/client";
import { Header } from './components/Header';
import { RecoilRoot } from 'recoil';

const App = () => {
  // where the database is located at
  const client = new ApolloClient({
    uri: 'http://it2810-14.idi.ntnu.no:4000/graphql',
    cache: new InMemoryCache()
});

  return (
    <RecoilRoot>
      <ApolloProvider client={client}>
          <SafeAreaView style={{flex:1 , backgroundColor: "#1E90FFFF"}}>
            <ScrollView>
            <View style={styles.pokemonDiv}>
                <Header />
                <PokemonsContainer />
            </View> 
          </ScrollView>
          </SafeAreaView> 
      </ApolloProvider>
    </RecoilRoot>
  );
}

const styles = StyleSheet.create({
  pokemonDiv: {
    flex: 1,
    backgroundColor: '#1E90FFFF',
  }});

export default App