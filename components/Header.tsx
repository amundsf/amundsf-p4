import React from 'react';
import { Image, Text,TextInput, StyleSheet, View } from "react-native";
import SelectDropdown from 'react-native-select-dropdown'
import {useRecoilState} from 'recoil'
import {searchState, capturedFilterState, typeState } from "../atoms/atoms";

export const Header = () => {
 
    // hooks states init
    const [searchTextValue, setSearchTextValue] = useRecoilState(searchState)
    const [capturedFiltering, setCapturedFiltering] = useRecoilState(capturedFilterState)
    const [type, setType] = useRecoilState(typeState)


    const SearchChange = (value: React.SetStateAction<string>) => {
        setSearchTextValue(value)
    };


  
    // all types of pokemons on the app
    const filterTypes = [
        "All",
        "Bug",
        "Dark", 
        "Dragon", 
        "Electric", 
        "Fairy", 
        "Fighting", 
        "Fire", 
        "Flying", 
        "Ghost", 
        "Grass", 
        "Ground", 
        "Ice", 
        "Normal", 
        "Poison", 
        "Psychic", 
        "Rock", 
        "Steel", 
        "Water"
    ];
    const CapturedChange  = (value: React.SetStateAction<string | boolean | null>) => {
        console.log(value)
        if (value == "All") {
            setCapturedFiltering(null);
        } else if (value == "Captured") {
            setCapturedFiltering(true)
        } else {
            setCapturedFiltering(false)
        }
    };

    const TypeChange = ( value: React.SetStateAction<string>) => {
        if (value === "All") {
            setType("");
        } else {
            setType(value);
        }
    };

    return (
            <View>
                <View style={styles.imgContainer}>
                    <Image style = {styles.pokemonImg} source={require("../resources/pokemonLogo.png")} />
                </View>
                <View style={styles.filteringContainer}>
                    <Text style={{fontSize: 20, marginBottom: 5}}>Types</Text>
                <SelectDropdown
                            buttonStyle={styles.DropdownFilter}
                            defaultValue={"All"}
                            data={filterTypes}
                            onSelect={(selectedItem) => {
                                TypeChange(selectedItem)
                            }}
                            buttonTextAfterSelection={(selectedItem) => {
                                // text after selection
                                return selectedItem
                            }}
                            rowTextForSelection={(item) => {
                                // text for each item
                                return item
                            }}
                        />
                        <Text style={{fontSize: 20, marginBottom: 5}}>Status</Text>
                        <SelectDropdown
                            buttonStyle={styles.DropdownFilter}
                            defaultValue={"All"}
                            data={["All", "Captured", "Not captured"]}
                            onSelect={(selectedItem) => {
                                CapturedChange(selectedItem)
                            }}
                            buttonTextAfterSelection={(selectedItem) => {
                                return selectedItem
                            }}
                            rowTextForSelection={(item) => {
                                return item
                            }}

                        />
                            <TextInput
                    style={styles.searchBox}
                    placeholder={"Search Pokemons"}
                    
                    value={searchTextValue}
                    onChangeText={(value) => SearchChange(value)}
                    ></TextInput>
                </View>

            </View>
    )
    
}

const styles = StyleSheet.create ({
    
    imgContainer: {
        flex: 1,
        alignItems: "center",
        margin: 5

    },
    pokemonImg: {
        flex: 1,
        width: 250,
        height: 110,
        resizeMode: 'contain'


    },
    filteringContainer: {
        borderColor: "black",
        maxWidth: "100%",
        minWidth: "85%",
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    DropdownFilter: {
        marginBottom: 5,
        borderColor: "black",
        borderWidth: 0.3,
        borderRadius: 10,
        width: 300
    },
    searchBox: {
        borderColor: "black",
        width: "50%",
        borderWidth: 0.3,
        borderRadius: 10,
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#FFE",
        height: 50,
        textAlign: 'center'
    }
})

export default Header

