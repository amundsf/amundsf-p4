import { useMutation } from '@apollo/react-hooks';
import React, { useState } from 'react';
import { StyleSheet, Image, Text, View, Pressable, TouchableWithoutFeedback } from 'react-native';
import { MUTATE_POKEMON } from '../graphql/get-pokemons';
import {Pokemon} from "../types/types";


const PokemonCard: React.FC<{ pokemon: Pokemon }> = ({pokemon}) => {

  const [clicked, setClicked] = useState(false);
  const [capturedPokemons, setCapturedPokemons] = useState(pokemon.captured);

  const [updateDatabase] = useMutation(MUTATE_POKEMON, {
      variables: {
          pokemonId: {
              id: pokemon.id
          }
      }
  })

  const handleClick = () => { // When user clicks on capture/captured!
    setClicked(!clicked)
};

  const updateDatabaseFunction = () => {
    setCapturedPokemons(!capturedPokemons)
    updateDatabase()
}

  return ( 
      <View style={styles.pokemon}>
        <View style={styles.pokemon__name}>
        <Text style = {styles.pokeman__name_text}>{pokemon.name}</Text>
        </View>
  
        {clicked && // if button is clicked show more info
         <View style={styles.pokemon__meta}>
         <Text style={styles.pokemon__meta_text}>#{pokemon.id}</Text>
         </View>
        }
        <TouchableWithoutFeedback onPress={handleClick}>
          <View style={styles.pokemon__image}>
          <Image style={styles.pokemon__image_img} source={{uri: pokemon.imgUrl}} />
          </View>
        </TouchableWithoutFeedback>

        {clicked && //if button clicked change button appearance
        <View style={styles.pokemon__type}>
        {pokemon.pokemonTypes.map((t: string) => (<Text style={styles.pokemon__type_text} key={t}> {t}</Text>))}
        </View>
        }
        
        
        <View >
          <Pressable style={[capturedPokemons ? styles.buttonPressed: styles.buttonNotPressed]} onPress={updateDatabaseFunction}>
            <Text style={{fontSize: 30, color: 'white'}}>{capturedPokemons ? "Captured!" : "Catch me!"}</Text>
           </Pressable>
        </View>
        
      </View>
    );
}


const styles = StyleSheet.create({
 pokemon: {
  marginBottom: 20,
  backgroundColor: '#fff',
  borderWidth: 1,
  borderColor: "rgba(0, 0, 0, 0.125)",
  borderRadius: 0.25,  
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,
  elevation: 4,
  overflow: 'hidden',
  
  justifyContent: 'space-between',
 },
  pokemon__name: {
    backgroundColor: '#ecd018',
    textAlign: 'center',
    alignItems: 'center',
    padding: 10,
    
  },
  pokeman__name_text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15,
  },

  pokemon__meta: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 0,
    padding: 0,
  },
  pokemon__meta_text: {
    backgroundColor: '#7bb7b7',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15,
    margin: 0,
    paddingTop: 5,
    paddingBottom: 10,
    borderRadius: 5,
    width: 50,
   textAlign: 'center',

  },
  pokemon__image: {
    padding: 15,
    flex: 1,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',

  },
  pokemon__image_img: {
    maxWidth: 120,
    maxHeight: 130,
    minHeight: 120,
    minWidth: 120,
    aspectRatio: 1,
    resizeMode: 'contain'

  },
  pokemon__type: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 15,
  },
  pokemon__type_text: {
    width: 100,
    minHeight: '25%',
    backgroundColor: 'lightskyblue',
    borderRadius: 3,
    
    fontWeight: '700',
    color: '#fff',
    
    fontSize: 12,
    marginBottom: 0,
    textAlign: 'center',
    
  },
  buttonNotPressed: {
    backgroundColor: '#17c589',
    width: "100%",
    paddingTop: 0,
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',

  },
  buttonPressed: {
    backgroundColor: '#FF6347',
    width: "100%",
    paddingTop: 0,
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',

  }
});

export default PokemonCard