export interface Pokemon {
    id: string | number;
    name: string;
    pokemonTypes: string[];
    imgUrl: string;
    captured: boolean;
}
